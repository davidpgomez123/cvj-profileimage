package com.javeriana.cvjprofileimage.service;

import com.javeriana.cvjprofileimage.dto.ProfileImageResponseDto;
import com.javeriana.cvjprofileimage.entity.ProfileImage;

public interface ProfileImageService {
    ProfileImageResponseDto getProfileImage(String code);
    String createUpdateProfileImage (ProfileImage profileImage);
}
