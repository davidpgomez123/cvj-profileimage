package com.javeriana.cvjprofileimage.service;

import com.javeriana.cvjprofileimage.dto.ProfileImageResponseDto;
import com.javeriana.cvjprofileimage.entity.ProfileImage;
import com.javeriana.cvjprofileimage.error.ProfileImageNotFound;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ProfileImageServiceImpl implements ProfileImageService{

    private final RepositoryProfileImageService repositoryProfileImageService;

    public ProfileImageServiceImpl(RepositoryProfileImageService repositoryProfileImageService) {
        this.repositoryProfileImageService = repositoryProfileImageService;
    }

    @Override
    public ProfileImageResponseDto getProfileImage(String code) {
        ProfileImage profileImage = repositoryProfileImageService.getProfileImage(code).orElse(null);
        if(Objects.nonNull(profileImage)){
            return new ProfileImageResponseDto(profileImage);
        }
        throw new ProfileImageNotFound();
    }

    @Override
    public String createUpdateProfileImage(ProfileImage profileImage) {
        ProfileImage profileImageFound = repositoryProfileImageService.getProfileImage(profileImage.getCode()).orElse(null);
        if(Objects.nonNull(profileImageFound)) {
            profileImageFound.setImage(profileImage.getImage());
            repositoryProfileImageService.saveProfileImage(profileImageFound);
        }else{
            repositoryProfileImageService.saveProfileImage(profileImage);
        }
        return "Ok";
    }
}
