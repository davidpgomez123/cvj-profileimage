package com.javeriana.cvjprofileimage.service;

import com.javeriana.cvjprofileimage.entity.ProfileImage;
import com.javeriana.cvjprofileimage.repository.ProfileImageRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RepositoryProfileImageServiceImpl implements RepositoryProfileImageService{

    private final ProfileImageRepository profileImageRepository;

    public RepositoryProfileImageServiceImpl(ProfileImageRepository profileImageRepository) {
        this.profileImageRepository = profileImageRepository;
    }

    @Override
    public void saveProfileImage(ProfileImage profileImage) {
        profileImageRepository.save(profileImage);
    }

    @Override
    public Optional<ProfileImage> getProfileImage(String code) {
        return profileImageRepository.findProfileImageByCode(code);
    }

}
