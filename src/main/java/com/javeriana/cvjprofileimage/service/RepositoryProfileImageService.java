package com.javeriana.cvjprofileimage.service;

import com.javeriana.cvjprofileimage.entity.ProfileImage;

import java.util.Optional;

public interface RepositoryProfileImageService {
    void saveProfileImage(ProfileImage profileImage);

    Optional<ProfileImage> getProfileImage(String code);
}
