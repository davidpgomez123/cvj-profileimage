package com.javeriana.cvjprofileimage.repository;


import com.javeriana.cvjprofileimage.entity.ProfileImage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ProfileImageRepository extends CrudRepository<ProfileImage, String> {

    @Query("SELECT t FROM ProfileImage t where t.code = :code")
    Optional<ProfileImage> findProfileImageByCode(@Param("code") String code);
}
