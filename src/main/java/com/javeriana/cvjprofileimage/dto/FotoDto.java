package com.javeriana.cvjprofileimage.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FotoDto {
    private Double ancho;
    private String imagen;
    private String tipoMime;
}
