package com.javeriana.cvjprofileimage.dto;

import com.javeriana.cvjprofileimage.entity.ProfileImage;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileImageResponseDto {

    public ProfileImageResponseDto(ProfileImage profileImage){
        this.image = profileImage.getImage();
    }
    private String image;
}
