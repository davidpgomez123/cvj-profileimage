package com.javeriana.cvjprofileimage.controller;

import com.javeriana.cvjprofileimage.dto.ProfileImageResponseDto;
import com.javeriana.cvjprofileimage.entity.ProfileImage;
import com.javeriana.cvjprofileimage.error.ProfileImageNotFound;
import com.javeriana.cvjprofileimage.service.ProfileImageService;
import com.javeriana.cvjprofileimage.service.ProfileImageServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin(
        origins = "*",
        methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.OPTIONS})
public class ProfileImageController {

    private final ProfileImageService profileImageService;

    public ProfileImageController(ProfileImageServiceImpl profileImageService) {
        this.profileImageService = profileImageService;
    }

    @GetMapping(value = "/profile_image")
    public ProfileImageResponseDto getProfileImage(@RequestParam String code){
        try{
            return profileImageService.getProfileImage(code);
        }catch (ProfileImageNotFound e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Profile Image Not Found");
        }
    }

    @PostMapping(value = "/profile_image")
    public String createProfileImage(@RequestBody ProfileImage profileImage){
        return profileImageService.createUpdateProfileImage(profileImage);
    }

}
