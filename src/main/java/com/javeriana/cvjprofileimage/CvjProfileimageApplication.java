package com.javeriana.cvjprofileimage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CvjProfileimageApplication {

	public static void main(String[] args) {
		SpringApplication.run(CvjProfileimageApplication.class, args);
	}

}
